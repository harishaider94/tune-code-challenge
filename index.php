<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
	<meta name="author" content="AdminKit">
	<meta name="keywords" content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Tune Code Challenge</title>
	<style>
		 input.transparent-input{
       /* background-color:rgba(0,0,0,0) !important; */
       opacity: 0.5;
       color:white;
    }
   
    .avatar {
  vertical-align: middle;
  width: 50px;
  height: 50px;
  border-radius: 50%;
}
	</style>
	<link href="css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
</head>

<body>
    <?php 
    if(!file_exists("./vendor/autoload.php"))
        {
            echo "<h1>Please run this command in terminal 'composer install'</h1>";
            die;
        }
    require_once 'vendor/autoload.php';
    use YoHang88\LetterAvatar\LetterAvatar;
    
    
    function js_str($s)
    {
        return '"' . addcslashes($s, "\0..\37\"\\") . '"';
    }
    
    function js_array($array)
    {
        $temp = array_map('js_str', $array);
        return '[' . implode(',', $temp) . ']';
    }
    
    $string = file_get_contents("users.json");
    $users = json_decode($string, true);
    $string = file_get_contents("logs.json");
    $logs = json_decode($string, true);
    $modifiedLog=[];
    // usort($users, function ($item1, $item2) {
    //     return $item1['name'] <=> $item2['name'];
    // });
    

    foreach($logs as $log)
    {
        $modifiedLog[$log['user_id']][$log['type']]=!empty($modifiedLog[$log['user_id']][$log['type']]) ? $modifiedLog[$log['user_id']][$log['type']]+1 : 1;
        if($log['type']=="conversion")
        {
            $times=date("m/d",strtotime($log['time']));
            $modifiedLog[$log['user_id']]["data"][$times]=!empty($modifiedLog[$log['user_id']]["data"][$times]) ? (float)$modifiedLog[$log['user_id']]["data"][$times]+(float)$log['revenue'] : (float)$log['revenue'];
        }
        $modifiedLog[$log['user_id']]['revenue']=!empty($modifiedLog[$log['user_id']]['revenue']) ? $modifiedLog[$log['user_id']]['revenue']+$log['revenue'] : $log['revenue'];
    }
    ?>
	<div class="wrapper">
		

		<div  class="main">
			<nav style="background: #3f51b5;color: #fff;" class="navbar navbar-expand navbar-light navbar-bg">
				<div>
					<i class="fa fa-bars" aria-hidden="true"></i>
					&nbsp;&nbsp;
					User Dashboard
				</div>

				<div class="navbar-collapse collapse">
					<ul class="navbar-nav navbar-align">
						<li class="nav-item dropdown">
							<input placeholder="Search" class="transparent-input" type="text">
						</li>
					</ul>
				</div>
			</nav>

			<main class="content">
				<div class="container-fluid p-0">

					<div class="row">
                    <?php foreach($users as $user){
                        $avatar = new LetterAvatar($user['name']);
                        $avatar=!empty($user['avatar']) ? $user['avatar'] : $avatar;

                        ksort($modifiedLog[$user['id']]['data']);
                        $datas=$modifiedLog[$user['id']]['data'];
                        $labels=array_keys($datas);
                       
                        ?>
						<div class="col-xl-4">
							<div class="card flex-fill w-100">
								<div class="card-header ">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <img  src="<?= $avatar ?>" class="img-fluid avatar" width="70">
                                        </div>
                                        <div class="col-lg-10">
                                        <h1 class="name"><?= $user['name'] ?></h1>
                                        <p class="occupation"><?= $user['occupation'] ?></p>
                                        </div>
                                    </div>
                            	</div>
								<div class="card-body py-3">
									<div class="row">
                                          <div class="col-lg-7 ">
                                          <canvas id="chartjs-dashboard-line-<?= $user['id'] ?>"></canvas>
                                          <label for="">Conversions <?= $labels[0] ?> - <?= end($labels) ?></label>
									
                                          </div>
                                          <div class="col-lg-5">
                                            <ul style="text-align:right;list-style: none;" >
                                                <li><?= $modifiedLog[$user['id']]['impression'] ?></li>
                                                <li>impressions</li>
                                                <li><?= $modifiedLog[$user['id']]['conversion'] ?></li>
                                                <li>conversions</li>
                                                <li>$<?= $modifiedLog[$user['id']]['revenue'] ?></li>
                                               <li>revenue</li>
                                            </ul>
									
                                          </div>    
                                    </div>
								</div>
							</div>
						</div>
                    <?php } ?>
					</div>

					

				</div>
			</main>

			<footer class="footer">
				<div class="container-fluid">
					<div class="row text-muted">
						<div class="col-6 text-start">
							<p class="mb-0">
								<a class="text-muted" href="#" target="_blank"><strong>Tune Challenge</strong></a> &copy;
							</p>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<script src="js/app.js"></script>

	<script>
         <?php foreach($users as $user){
             $datas=$modifiedLog[$user['id']]['data'];
             $labels=array_keys($datas);
             $data=array_values($datas);
             echo 'var labels = ', js_array($labels), ';';
             echo 'var data = ', js_array($data), ';';

             ?>

        new Chart(document.getElementById("chartjs-dashboard-line-<?= $user['id'] ?>"), {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{ 
                    data: data,
                    label: "",
                    borderColor: "#a5b3ff",
                    fill: false,
                    pointRadius: 5,
                }
                ]
            },
            options: {
                title: {
                display: false,
                },
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                            display: false,
                        },
                        ticks: {
                        display: false, //this removed the labels on the x-axis
                        
                        },
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                            display: false,
                        },
                        ticks: {
                        display: false, //this removed the labels on the x-axis
                        },   
                    }]
                }

            }
            });
            <?php } 
             ?>

    </script>

</body>

</html>